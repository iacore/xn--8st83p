const std = @import("std");

fn add_modules(b: *std.Build, exe: *std.Build.LibExeObjStep) void {
    const jzignet = b.dependency("jzignet", .{ .target = exe.target, .optimize = exe.optimize });
    exe.addModule("janet", jzignet.module("jzignet"));
    exe.addModule("cjanet", jzignet.module("cjanet"));
    exe.linkLibrary(jzignet.artifact("jzignet"));

    exe.addModule("httpz", b.dependency("httpz", .{}).module("httpz"));
}

// fn git_HEAD() []const u8 {
//     std.os.Command
//     git rev-parse HEAD
// }

// idea: mark every sentence group claimed by name-githash

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    {
        const exe = b.addExecutable(.{
            .name = "8st83p",

            .root_source_file = .{ .path = "src/main.zig" },
            .target = target,
            .optimize = optimize,
        });
        add_modules(b, exe);

        b.installArtifact(exe);

        const run_cmd = b.addRunArtifact(exe);

        run_cmd.step.dependOn(b.getInstallStep());

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("run", "Run the app");
        run_step.dependOn(&run_cmd.step);

        const unit_tests = b.addTest(.{
            .root_source_file = .{ .path = "src/main.zig" },
            .target = target,
            .optimize = optimize,
        });
        add_modules(b, unit_tests);

        const run_unit_tests = b.addRunArtifact(unit_tests);

        const test_step = b.step("test", "Run unit tests");
        test_step.dependOn(&run_unit_tests.step);
        test_step.dependOn(&exe.step);
    }

    {
        const exe = b.addExecutable(.{
            .name = "8st83p-readme",

            .root_source_file = .{ .path = "src/main_readme.zig" },
            .target = target,
            .optimize = optimize,
        });
        add_modules(b, exe);

        b.installArtifact(exe);

        const run_cmd = b.addRunArtifact(exe);

        run_cmd.step.dependOn(b.getInstallStep());

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("readme", "print the intro paragraph to be put in readme.md");
        run_step.dependOn(&run_cmd.step);
    }
}
