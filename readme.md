# 星布

```tuplese
[ (: :8st83p :name "8st83p")
  (:0 :8st83p :act :handle_http)
  (:1 :8st83p :act :recall_memory)
  (:2 :8st83p :act :se)
  (: :0 :canonical "externally managed") ]
```

## config

Edit [src/config.zig](src/config.zig).

## data types

- nothing: `_`
- literal (not indexed): `"lit"`
- reference (indexed): `:ref`

## creator's comments

pronouns.cc doesn't support recursive membership relations, so we made this.

there is no grammar; how you interpret the data is up to you.

one common format: (:id subject relation object modifier/identifier)

the thing does have a built-in dictionary at [src/se.zig](src/se.zig), which you may imitate.
