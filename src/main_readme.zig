const se = @import("se.zig");
const imagine = se.imagine;
const D = se.D;
const lit = se.lit;
const ref = se.ref;

pub fn main() !void {
    imagine()
        .c(null, .{ D.self, D.name, lit(D.self.ref.s) })
        .c(ref(0), .{ D.self, D.act, D.serve_request })
        .c(ref(1), .{ D.self, D.act, D.recall_memory })
        .c(ref(2), .{ D.self, D.act, D.se })
        .c(null, .{ ref(0), D.canonically, lit("externally managed") })
        .state();
}
