const std = @import("std");
const httpz = @import("httpz");
const Indexer = @import("Indexer.zig");
const Concept = @import("se.zig").Concept;
const Ref = @import("se.zig").Ref;

pub fn query_by_id_plain_text(res: *httpz.Response, data: Indexer.LookupResult) !void {
    const writer = res.writer();
    res.header("content-type", "text/plain");
    try writer.print(
        \\# definition
        \\
        \\
    , .{});
    if (data.definition) |concept| {
        try writer.print(
            \\{}
            \\
        , .{concept});
    }
    try writer.print(
        \\
        \\# referenced_in
        \\
        \\
    , .{});
    for (data.referenced_in) |item| {
        try writer.print(
            \\{} {}
            \\
        , .{ item.concept, item.member_index });
    }
    try writer.print(
        \\
        \\
    , .{});
}

pub fn query_by_id(res: *httpz.Response, data: Indexer.LookupResult) !void {
    const writer = res.writer();
    res.header("content-type", "application/xhtml+xml");
    try writer.print(
        \\<?xml version="1.0" encoding="UTF-8"?>
        \\<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="janet data notation" lang="janet data notation">
        \\<head>
        \\    <meta charset="UTF-8"/>
        \\    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        \\    <title>(: "page" "has" "id" {}) (: "self" "is_type_of" "quadstore")</title>
        \\</head>
    , .{data.id});
    try writer.print(
        \\<body>
        \\    <h1>{}</h1>
        \\    <h2>definition</h2>
        \\    <div class="definition">
    , .{data.id});
    if (data.definition) |concept| {
        try writer.print(
            \\    <div><code>{html}</code></div>
            \\
        , .{concept});
    }
    try writer.print(
        \\    </div>
        \\    <h2>referenced_in</h2>
        \\    <div class="referenced_in">
    , .{});
    for (data.referenced_in) |item| {
        try writer.print(
            \\    <div><code>{html}</code> <code>{}</code></div>
        , .{ item.concept, item.member_index });
    }
    try writer.print(
        \\    </div>
        \\</body>
        \\</html>
    , .{});
}

pub fn writeAllEscaped(writer: anytype, s: []const u8) !void {
    for (s) |char| {
        switch (char) {
            '&' => try writer.writeAll("&#38;"),
            '<' => try writer.writeAll("&lt;"),
            '>' => try writer.writeAll("&gt;"),
            '"' => try writer.writeAll("&#34;"),
            '\'' => try writer.writeAll("&#39;"),
            else => try writer.writeByte(char),
        }
    }
}
