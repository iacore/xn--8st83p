const std = @import("std");
const logz = @import("logz");
const httpz = @import("httpz");
const mustache = @import("mustache");
const Parser = @import("Parser.zig");
const Indexer = @import("Indexer.zig");
const FileWatcher = @import("FileWatcher.zig");
const se = @import("se.zig");
const Concept = se.Concept;
const ThoughtCollector = se.ThoughtCollector;
const D = se.builtin_dictionary;
const Ref = se.Ref;
const ref = se.ref;
const lit = se.lit;
const c = se.c;
const imagine = se.imagine;
const templates = @import("templates.zig");

test {
    _ = Parser;
    _ = Indexer;
    _ = FileWatcher;
    _ = se;
}

const Config = @import("config.zig");
const limits = @import("config.zig").limits;

const Context = struct {
    a: std.mem.Allocator,
    config: Config,
    parser: Parser,
    watcher: FileWatcher,
    indexer: Indexer,

    pub fn init(a: std.mem.Allocator, config: Config) !@This() {
        const parser = try Parser.init();
        errdefer parser.deinit();
        const watcher = try FileWatcher.init(a, config.statements_path);
        errdefer watcher.deinit();
        return .{
            .a = a,
            .config = config,
            .parser = parser,
            .watcher = watcher,
            .indexer = Indexer.init(a),
        };
    }
    pub fn deinit(ctx: *@This()) void {
        defer ctx.parser.deinit();
        defer ctx.indexer.deinit();
        defer ctx.watcher.deinit();
    }

    fn loadAllFiles(this: *@This(), thought_collector: *ThoughtCollector) !void {
        this.indexer.reset();

        var dir_stmts = try std.fs.cwd().openIterableDir(this.config.statements_path, .{});
        defer dir_stmts.close();
        var it = dir_stmts.iterate();
        while (try it.next()) |entry| {
            if (entry.kind == .file) {
                if (std.mem.startsWith(u8, entry.name, ".")) continue;
                var thought = imagine()
                    .c(ref(0), .{ D.self, D.act, D.load_statement_file })
                    .c(null, .{ ref(0), D.name, lit(entry.name) });

                this.loadFile(dir_stmts.dir, entry.name, thought_collector) catch |e| {
                    thought = thought.c(null, .{ ref(0), D.error_code, e });
                };
                try thought_collector.collect(thought);
            }
            if (entry.kind == .sym_link) {
                // awa?
            }
        }

        try this.indexer.reindex_member_ref();
    }

    fn loadFile(this: *@This(), dir: std.fs.Dir, filename: []const u8, thought_collector: *ThoughtCollector) !void {
        const f = try dir.openFile(filename, .{});
        defer f.close();
        const source = try f.readToEndAlloc(this.a, limits.source_file_size);
        defer this.a.free(source);
        const statements = try this.parser.parse_statements(this.a, source);
        defer statements.deinit();
        for (statements.items) |item| {
            const concept = Parser.parse(item) catch {
                const item_s = this.parser.pretty_print(item) catch unreachable; // i'm too lazy to handle janet errors. crash and restart for now.
                try thought_collector.collect(imagine()
                    .c(ref(0), .{ D.self, D.act, D.parse_statement })
                    .c(null, .{ ref(0), D.canonically, lit(item_s) })
                    .c(null, .{ ref(0), D.error_code, error.invalid_source_concept_term }));
                continue;
            };
            try this.indexer.add(concept);
        }
        const thought = imagine()
            .c(ref(0), .{ D.self, D.act, D.parse_statement })
            .c(null, .{ ref(0), D.len, statements.items.len });
        try thought_collector.collect(thought);
    }
};

var server: httpz.ServerCtx(*Context, *Context) = undefined;

fn interrupt(_: c_int, _: *const std.os.siginfo_t, _: ?*const anyopaque) callconv(.C) void {
    server.stop();
}

// - parse files
// - index refs
// - set up web interface
pub fn main() !void {
    var config = Config{};
    if (std.os.getenv("UNIXPATH")) |path| {
        config.unix_socket = path;
    } else {
        if (std.os.getenv("HOST")) |host| {
            config.host = host;
        }
        if (std.os.getenv("PORT")) |port| {
            config.port = try std.fmt.parseInt(u16, port, 10);
        }
    }

    imagine()
        .c(null, .{ Ref.init("self"), D.name, D.self })
        .state();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    var ctx = try Context.init(a, config);
    defer ctx.deinit();

    server = if (config.unix_socket) |socket_path| 
        try @TypeOf(server).init(a, .{ .unix_path = socket_path }, &ctx)
    else 
        try @TypeOf(server).init(a, .{ .address = config.host, .port = config.port }, &ctx);
    defer server.deinit();

    // overwrite the default notFound handler
    server.notFound(notFound);

    // overwrite the default error handler
    server.errorHandler(errorHandler);

    var router = server.router();

    // use get/post/put/head/patch/options/delete
    // you can also use "all" to attach to all methods
    router.get("/", indexPage);
    router.get("/:*", getConcept);

    inline for (.{ .TERM, .INT }) |signal| {
        std.os.sigaction(@field(std.os.SIG, @tagName(signal)), &.{ .handler = .{ .sigaction = &interrupt }, .mask = std.os.empty_sigset, .flags = 0 }, null) catch |e| {
            imagine()
                .c(ref(0), .{ D.self, D.act, D.sigaction })
                .c(null, .{ ref(0), D.name, lit("SIG" ++ @tagName(signal)) })
                .c(null, .{ D.self, D.error_code, e })
                .state();
        };
    }

    if (config.unix_socket) |socket_path| {
        imagine()
            .c(ref(0), .{ D.self, D.act, D.listen })
            .c(null, .{ ref(0), D.unixpath, lit(socket_path) })
            .state();
        _ = std.c.umask(0o117);
    } else {
        const listening_on_url = try std.fmt.allocPrint(a, "http://{s}:{}", .{ server.config.address.?, server.config.port.? });
        defer a.free(listening_on_url);

        imagine()
            .c(ref(0), .{ D.self, D.act, D.listen })
            .c(null, .{ ref(0), D.url, lit(listening_on_url) })
            .state();
    }

    // start the server in the current thread, blocking.
    try server.listen();

    imagine()
        .c(ref(0), .{ D.self, D.act, D.eep })
        .state();
}

// how httpz request handlers work
//
// status code 200 is implicit.
//
// you can set the body directly to a []u8, but note that the memory
// must be valid beyond your handler. Use the res.arena if you need to allocate
// memory for the body.

fn indexPage(_: *Context, _: *httpz.Request, res: *httpz.Response) !void {
    try res.writer().print("{}\n", .{c(null, .{ D.self, D.say, lit("hello!") })});
    try res.writer().print("{}\n", .{c(null, .{ D.self, D.url, lit("https://codeberg.org/iacore/xn--8st83p") })});
}

fn requestedContentType(req: *httpz.Request) enum { html, plain, json } {
    if (req.header("accept")) |accept| {
        var it = std.mem.split(u8, accept, ",");
        while (it.next()) |mime| {
            if (std.mem.eql(u8, mime, "text/html")) return .html;
            if (std.mem.eql(u8, mime, "text/plain")) return .plain;
            if (std.mem.eql(u8, mime, "application/json")) return .json;
        }
    }
    return .plain;
}

fn getConcept(ctx: *Context, req: *httpz.Request, res: *httpz.Response) !void {
    if (std.mem.startsWith(u8, req.url.path, "/:")) {
        const id = try res.arena.dupe(u8, req.url.path[2..]);
        {
            var thought_collector = try ThoughtCollector.init(res.arena);
            defer thought_collector.deinit();
            try ctx.loadAllFiles(&thought_collector);
            for (thought_collector.groups_of_thoughts.items) |thoughts| {
                thoughts.state();
            }
        }
        const data = try ctx.indexer.lookupLeaky(res.arena, Ref.init(id));
        switch (requestedContentType(req)) {
            .html => {
                try templates.query_by_id(res, data);
            },
            .plain => {
                try templates.query_by_id_plain_text(res, data);
            },
            .json => {
                try res.json(data, .{});
            },
        }
    } else {
        return notFound(ctx, req, res);
    }
}

fn notFound(_: *Context, _: *httpz.Request, res: *httpz.Response) !void {
    res.status = 404;

    try res.writer().print("{}\n", .{c(null, .{ D.self, D.say, lit("not found!") })});
}

// note that the error handler return `void` and not `!void`
fn errorHandler(_: *Context, req: *httpz.Request, res: *httpz.Response, err: anyerror) void {
    res.status = 500;

    res.writer().print("{}\n", .{c(null, .{ D.self, D.say, lit("ouch") })}) catch {};

    imagine()
        .c(ref(0), .{ D.self, D.act, D.serve_request })
        .c(null, .{ ref(0), D.url, lit(req.url.raw) })
        .c(null, .{ ref(0), D.error_code, err })
        .state();
}
