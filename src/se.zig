//! Ability To Self-Express
//!
//! "There are a lot of boxing taking place in this room, <redacted>."
//! "awa?"
//! "I dunno. Maybe they like to be put in boxes, just like you."

const std = @import("std");
const FieldType = std.meta.FieldType;
const limits = @import("config.zig").limits;
const writeAllEscaped = @import("templates.zig").writeAllEscaped;

// it allocates temporary memory
// hack: this part doesn't feel quite right. it should work if not too much memory is used at once. it also relies on the fact that the allocated strings are short-lived.
// for unimportant data that may be corrupted without affecting the program's behavior
var cyclic_buffer: [4096]u8 = undefined;
var cyclic_buffer_allocator_impl = @import("stdpatch/heap.zig").CyclicBufferAllocator.init(&cyclic_buffer);
const cyclic_buffer_allocator = cyclic_buffer_allocator_impl.allocator();

/// smallest unit of concept (under this framework)
///
/// One thing I learned from studying linguistics is that every word should be a reference, not a literal. It is ok for most words to be undefined.
pub const Word = struct {
    ref: Ref,
    en_common: ?[]const u8,
};

pub fn word(ref_name: []const u8, en_common: ?[]const u8) Word {
    return Word{
        .ref = Ref.init(ref_name),
        .en_common = en_common,
    };
}

pub fn c(name: ?Ref, fields: anytype) Concept {
    var concept = Concept.init(name, undefined);
    inline for (fields, 0..) |x, i| {
        concept.m[i] = coerce(x);
    }
    return concept;
}

/// All words used by :8st83p
/// the identifier names are from the perspective of it
///
/// Some interesting grammar evolved from this:
///   [ self act ~ ] is usually marked by ref(0)
pub const builtin_dictionary = struct {
    pub const self = word("8st83p", "this thing");
    pub const unixpath = word("unixpath", "unix socket path");
    pub const url = word("url", "URL");
    pub const name = word("name", "is called");
    pub const len = word("len", "has count of");
    pub const canonically = word("canonical", "canonically described as"); // (: ~ :canonical "literal")
    pub const error_code = word("error_code", "failed with error code");

    pub const say = word("say", "say"); // not used internally

    pub const act = word("act", "did a thing");

    // actions
    pub const eep = word("eep", "eep");
    pub const watch_statements = word("watch_st_all", "watch statements");
    pub const reload_statements = word("reload_st_all", "reload statements");
    pub const load_statement_file = word("load_st_multi", "load statement file");
    pub const parse_statement = word("parse_st", "parse statement in file");
    pub const sigaction = word("sigaction", "setup signal handler");
    pub const serve_request = word("handle_http", "serve http request");
    pub const listen = word("listen_http", "listen for http requests");
    pub const recall_memory = word("recall_memory", "recall from memory");
    pub const se = word("se", "self express");
};
pub const D = builtin_dictionary;

pub fn ref(comptime id: comptime_int) Ref {
    return .{ .s = std.fmt.comptimePrint("{}", .{id}) };
}
pub const lit = Term.lit;

fn isFormatHtml(comptime fmt: []const u8) bool {
    if (fmt.len == 0) {
        return false;
    } else if (std.mem.eql(u8, fmt, "html")) {
        return true;
    } else {
        std.debug.panic("Invalid fmt: {s}", .{fmt});
    }
}

/// reference to a concept
pub const Ref = struct {
    s: []const u8,

    pub fn init(s: []const u8) @This() {
        return .{ .s = s };
    }

    pub fn format(this: ?@This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        const isHtml = isFormatHtml(fmt);
        if (this) |ies| {
            if (isHtml) {
                try writer.writeAll(
                    \\<a href="
                );
                try writer.writeAll("/:");
                try writeAllEscaped(writer, ies.s);
                try writer.writeAll(
                    \\">
                );
                try writer.writeAll(":");
                try writeAllEscaped(writer, ies.s);
                try writer.writeAll(
                    \\</a>
                );
            } else {
                try writer.writeAll(":");
                try writer.writeAll(ies.s);
            }
        } else {
            try writer.writeAll(":");
        }
    }
};

pub const Term = union(enum) {
    /// no value
    _,
    reference: Ref,
    literal: []const u8,

    pub fn ref(s: []const u8) @This() {
        return .{ .reference = .{ .s = s } };
    }
    pub fn lit(s: []const u8) @This() {
        return .{ .literal = s };
    }

    pub fn format(this: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        const isHtml = isFormatHtml(fmt);
        var buf: [4096]u8 = undefined;
        switch (this) {
            ._ => try writer.writeAll("_"),
            .reference => |reference| try reference.format(fmt, options, writer),
            .literal => |s| if (isHtml) {
                var fbs = std.io.fixedBufferStream(&buf);
                var hasError = false;
                std.json.encodeJsonString(s, .{}, fbs.writer()) catch |e|
                    switch (e) {
                    error.NoSpaceLeft => hasError = true,
                    else => |e_| return e_,
                };
                if (hasError) {
                    try writer.writeAll("__ERR_LITERAL_TOO_LONG__");
                } else {
                    try writeAllEscaped(writer, fbs.getWritten());
                }
            } else {
                try std.json.encodeJsonString(s, .{}, writer);
            },
        }
    }
};

/// memory will be shared with the underlying Janet data
pub const Concept = struct {
    id: ?Ref,
    m: [limits.concept_member_count]Term,
    pub fn init(id: ?Ref, m: [limits.concept_member_count]Term) @This() {
        return .{ .id = id, .m = m };
    }
    pub fn eql(lhs: @This(), rhs: @This()) bool {
        return @import("stdpatch/meta.zig").eqlWithSlice(lhs, rhs);
    }
    pub fn format(this: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        try writer.writeAll("(");
        try Ref.format(this.id, fmt, options, writer);
        // don't print the trailing _ fields
        var last_boundary = this.m.len;
        while (last_boundary > 0) {
            if (this.m[last_boundary - 1] == ._) {
                last_boundary -= 1;
            } else {
                break;
            }
        }
        for (this.m[0..last_boundary]) |member| {
            try writer.writeAll(" ");
            try member.format(fmt, options, writer);
        }
        try writer.writeAll(")");
    }
};

test "trailing _ fields should not be printed" {
    const a = std.testing.allocator;
    {
        const buf = try std.fmt.allocPrint(a, "{}", .{c(null, .{ ._, ._, ._, ._ })});
        defer a.free(buf);
        try std.testing.expectEqualStrings("(:)", buf);
    }
    {
        const buf = try std.fmt.allocPrint(a, "{}", .{c(null, .{ lit("awa"), ._, ._, ._ })});
        defer a.free(buf);
        try std.testing.expectEqualStrings("(: \"awa\")", buf);
    }
    {
        const buf = try std.fmt.allocPrint(a, "{}", .{c(null, .{ lit("awa"), lit("awa"), lit("awa"), lit("awa") })});
        defer a.free(buf);
        try std.testing.expectEqualStrings("(: \"awa\" \"awa\" \"awa\" \"awa\")", buf);
    }
}

pub fn coerce(d: anytype) Term {
    const T = @TypeOf(d);
    if (T == Term)
        return d;
    if (T == Ref)
        return .{ .reference = d };
    if (T == *const Concept)
        return .{ .direct = d };
    if (T == Word)
        return .{ .reference = d.ref }; // idea: add language setting after adding more languages
    switch (@typeInfo(T)) {
        .Int => {
            return .{ .literal = std.fmt.allocPrint(cyclic_buffer_allocator, "{}", .{d}) catch "<int:memory full>" };
        },
        .ErrorSet => {
            return .{ .literal = std.fmt.allocPrint(cyclic_buffer_allocator, "error.{}", .{d}) catch "error.<memory full>" };
        },
        .Pointer, .Array => {
            @compileLog("using literal directly is not allowed: ", d);
        },
        else => {},
    }
    // @compileError(std.fmt.comptimePrint("invalid type of d: {}", .{T}));
    return d;
}

pub const LimitedThoughts = struct {
    storage: std.BoundedArray(Concept, 16) = .{},

    pub const head_empty = @This(){};

    pub fn _and(this_: @This(), concept: Concept) @This() {
        var this = this_;
        this.storage.append(concept) catch @panic("stop thinking too much");
        return this;
    }

    pub fn state(this: @This()) void {
        const stderr = std.io.getStdErr();
        stderr.lock(.exclusive) catch return;
        defer stderr.unlock();
        stderr.writer().print("{}", .{this}) catch {
            // cannot scream, sad :(
        };
    }

    pub fn format(this: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        try writer.writeAll("[");
        const slice = this.storage.slice();
        for (slice, 0..) |concept, i| {
            const is_first = i == 0;
            const is_last = i + 1 == slice.len;
            if (is_first)
                try writer.writeAll(" ")
            else
                try writer.writeAll("  ");
            try concept.format(fmt, options, writer);
            if (is_last)
                try writer.writeAll(" ")
            else
                try writer.writeAll("\n");
        }
        try writer.writeAll("]\n");
    }

    pub fn c(this: @This(), name: ?Ref, fields: anytype) @This() {
        var concept = Concept.init(name, undefined);
        inline for (fields, 0..) |x, i| {
            concept.m[i] = coerce(x);
        }
        return this._and(concept);
    }
};

pub fn imagine() LimitedThoughts {
    return LimitedThoughts.head_empty;
}

pub const ThoughtCollector = struct {
    groups_of_thoughts: std.ArrayList(LimitedThoughts),

    pub fn init(a: std.mem.Allocator) !@This() {
        return .{
            .groups_of_thoughts = FieldType(@This(), .groups_of_thoughts).init(a),
        };
    }

    pub fn deinit(this: *@This()) void {
        this.groups_of_thoughts.deinit();
    }

    pub fn collect(this: *@This(), thoughts: LimitedThoughts) !void {
        try this.groups_of_thoughts.append(thoughts);
    }
};
