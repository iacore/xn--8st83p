const std = @import("std");
const limits = @import("config.zig").limits;
const Concept = @import("se.zig").Concept;
const Ref = @import("se.zig").Ref;
const FieldType = std.meta.FieldType;

pub const ConceptMemberRef = struct {
    i: usize,
    member_index: usize, // optimize: could be u2
};
pub const ConceptMemberRefList = std.ArrayList(ConceptMemberRef);

concept_list: std.ArrayList(Concept),
indices_by_id: std.StringArrayHashMap(usize),
indices_by_member_ref: std.StringArrayHashMap(ConceptMemberRefList),

pub fn init(a: std.mem.Allocator) @This() {
    return .{
        .concept_list = FieldType(@This(), .concept_list).init(a),
        .indices_by_id = FieldType(@This(), .indices_by_id).init(a),
        .indices_by_member_ref = FieldType(@This(), .indices_by_member_ref).init(a),
    };
}

pub fn deinit(this: *@This()) void {
    this.concept_list.deinit();
    this.indices_by_id.deinit();
    this.free_member_ref();
    this.indices_by_member_ref.deinit();
}

pub fn reset(this: *@This()) void {
    this.concept_list.clearRetainingCapacity();
    this.indices_by_id.clearRetainingCapacity();
    this.free_member_ref();
    this.indices_by_member_ref.clearRetainingCapacity();
}

pub fn add(this: *@This(), concept: Concept) !void {
    if (concept.id) |id| {
        const i = this.concept_list.items.len;
        try this.concept_list.append(concept);
        errdefer _ = this.concept_list.pop();
        const kv = try this.indices_by_id.getOrPut(id.s);
        if (kv.found_existing) {
            std.log.warn("duplicate id: {s}", .{id.s});
            // return error.Concept_DuplicateId;
        } else {
            kv.value_ptr.* = i;
        }
    } else {
        try this.concept_list.append(concept);
    }
}

pub fn free_member_ref(this: *@This()) void {
    for (this.indices_by_member_ref.values()) |value| {
        value.deinit();
    }
    this.indices_by_member_ref.clearRetainingCapacity();
}

/// if it fails, `indices_by_member_ref` is poisoned
pub fn reindex_member_ref(this: *@This()) !void {
    this.free_member_ref();

    for (this.concept_list.items, 0..) |concept, i| {
        for (concept.m, 0..) |member, member_index| {
            switch (member) {
                .reference => |ref| {
                    const kv = try this.indices_by_member_ref.getOrPut(ref.s);
                    if (!kv.found_existing) {
                        kv.value_ptr.* = ConceptMemberRefList.init(this.indices_by_member_ref.allocator);
                    }
                    try kv.value_ptr.append(.{ .i = i, .member_index = member_index });
                },
                else => {},
            }
        }
    }
}

pub const LookupResult = struct {
    pub const Item = struct { concept: Concept, member_index: usize };
    id: Ref,
    definition: ?Concept,
    referenced_in: []Item,
};

pub fn lookupLeaky(this: @This(), arena: std.mem.Allocator, id: Ref) !LookupResult {
    const concepts = this.concept_list.items;
    const maybe_exact_match = this.indices_by_id.get(id.s);
    const maybe_referenced_in = this.indices_by_member_ref.get(id.s);
    var ref_list = std.ArrayList(LookupResult.Item).init(arena);
    if (maybe_referenced_in) |referenced_in| {
        for (referenced_in.items) |item| {
            try ref_list.append(.{
                .concept = concepts[item.i],
                .member_index = item.member_index,
            });
        }
    }
    const c_exact_match: ?Concept = if (maybe_exact_match) |exact_match| concepts[exact_match] else null;
    return .{
        .id = id,
        .definition = c_exact_match,
        .referenced_in = try ref_list.toOwnedSlice(),
    };
}

test "dry run" {
    var awa = init(std.testing.allocator);
    defer awa.deinit();
    try awa.reindex_member_ref();
}
