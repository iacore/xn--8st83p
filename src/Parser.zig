//! syntax design
//! =============
//!
//! keyword: id
//! string: literal
//! symbol: special functions or names
//!
//! special symbols
//! _   empty / placeholder
//!
//! file is a namespace? no. namespaces there are none.
//! id prefix can act like namespaces, but this is not indexed
//! e.g. :iacore/awawa , :iacore/ is prefix
//!
//! special ids
//! :__avatar__ :__name__ :__description__
//! or maybe "__avatar__" will work better, since it's not indexed

const std = @import("std");
const FieldType = std.meta.FieldType;
const logz = @import("logz");
const janet = @import("janet");
const Janet = janet.Janet;
const c = @import("cjanet");
const se = @import("se.zig");
const Concept = se.Concept;
const IdOrStringOrNull = se.Term;
const Ref = se.Ref;
const Term = se.Term;

vm: *c.JanetVM,

/// init parser
pub fn init() !@This() {
    // I want Janet to have a context type that can be passed around freely, but there is none.
    // setup janet instance
    try janet.init();
    const vm: *c.JanetVM = c.janet_vm_alloc() orelse return error.OutOfMemory;
    c.janet_vm_save(vm);
    return .{
        .vm = vm,
    };
}

/// release memory use, invalidate all concepts associated with it
pub fn deinit(this: @This()) void {
    c.janet_vm_load(this.vm);
    janet.deinit();
    c.janet_vm_free(this.vm);
}

pub fn pretty_print(this: @This(), data: Janet) ![]const u8 {
    c.janet_vm_load(this.vm);
    defer c.janet_vm_save(this.vm);

    const env = janet.Environment.coreEnv(null);
    env.def("d", data, null);
    const s = try env.doString(
        \\(string/format "%q" d)
    , @src().fn_name);
    return (try s.bytesView()).slice();
}

/// parse and root the values
pub fn parse_statements(this: @This(), a: std.mem.Allocator, input: []const u8) !std.ArrayList(Janet) {
    c.janet_vm_load(this.vm);
    defer c.janet_vm_save(this.vm);

    var parser = janet.Parser.init();
    defer parser.deinit();
    for (input) |ch| {
        parser.consume(ch);
        switch (parser.status()) {
            .root => {},
            .pending => {},
            .dead => unreachable,
            .@"error" => {
                std.log.err("{s}", .{parser.@"error"().?});
                return error.JanetParseError;
            },
        }
    }
    parser.eof();

    var res = std.ArrayList(Janet).init(a);
    errdefer res.deinit();
    while (parser.hasMore()) {
        const parsed_value = parser.produce();
        janet.gcRoot(parsed_value);
        try res.append(parsed_value);
        if (parser.status() == .@"error") {
            std.log.err("{s}", .{parser.@"error"().?});
            std.log.err("no value when it should be", .{});
            unreachable;
        }
    }
    return res;
}

pub const parseError = error.Concept_InvalidSource;
pub const ParseError = error{Concept_InvalidSource};

fn parseFail() ParseError {
    return parseError;
}

fn parseItem(items: []const Janet, i: usize) ParseError!IdOrStringOrNull {
    if (i < items.len) {
        const v = items[i];
        switch (v.janetType()) {
            .keyword => {
                const s = (v.bytesView() catch unreachable).slice();
                return Term.ref(s);
            },
            .string => {
                const s = (v.bytesView() catch unreachable).slice();
                return Term.lit(s);
            },
            .symbol => {
                const s = (v.bytesView() catch unreachable).slice();
                if (std.mem.eql(u8, s, "_")) {
                    return ._;
                } else {
                    return parseFail();
                }
            },
            else => |invalid_type| {
                std.log.err("invalid concept element type: {}", .{invalid_type});
                return parseFail();
            },
        }
    } else {
        return ._;
    }
}

pub fn parse(data: Janet) ParseError!Concept {
    tt.expectEqual(Janet.Type.tuple, data.janetType()) catch return parseError;
    const tuple = data.indexedView() catch unreachable;
    tt.expect(tuple.len >= 1) catch return parseError;
    tt.expect(tuple.len <= 1 + @typeInfo(std.meta.FieldType(Concept, .m)).Array.len) catch return parseError;
    const items = tuple.slice();
    tt.expectEqual(Janet.Type.keyword, items[0].janetType()) catch return parseError;
    const id_ = items[0].bytesView() catch unreachable;
    const id__ = id_.slice();
    var m: FieldType(Concept, .m) = undefined;
    for (&m, 1..) |*member, i| {
        member.* = try parseItem(items, i);
    }
    return .{
        .id = if (id__.len == 0) null else .{ .s = id__ },
        .m = m,
    };
}

const tt = std.testing;

test "test parse" {
    const parser = try init();
    defer parser.deinit();
    try parser.test_parse(
        \\(:)
    , Concept.init(null, .{ ._, ._, ._, ._ }));
    try parser.test_parse(
        \\(:vvvvvvvvv)
    , Concept.init(Ref.init("vvvvvvvvv"), .{ ._, ._, ._, ._ }));
    try parser.test_parse(
        \\(: :a)
    , Concept.init(null, .{ Term.ref("a"), ._, ._, ._ }));
    try parser.test_parse(
        \\(: :a :b)
    , Concept.init(null, .{ Term.ref("a"), Term.ref("b"), ._, ._ }));
    try parser.test_parse(
        \\(: :a :b :c)
    , Concept.init(null, .{ Term.ref("a"), Term.ref("b"), Term.ref("c"), ._ }));
    try parser.test_parse(
        \\(: :a :b :c :d)
    , Concept.init(null, .{ Term.ref("a"), Term.ref("b"), Term.ref("c"), Term.ref("d") }));
    try parser.test_parse(
        \\(:awawa :a :b :c)
    , Concept.init(Ref.init("awawa"), .{ Term.ref("a"), Term.ref("b"), Term.ref("c"), ._ }));
    try parser.test_parse(
        \\(: :a _ :c)
    , Concept.init(null, .{ Term.ref("a"), ._, Term.ref("c"), ._ }));
    try parser.test_parse(
        \\(: _ _ :c :d)
    , Concept.init(null, .{ ._, ._, Term.ref("c"), Term.ref("d") }));
    try parser.test_parse(
        \\(: _ _ "c" :d)
    , Concept.init(null, .{ ._, ._, Term.lit("c"), Term.ref("d") }));
}

fn test_parse(this: @This(), input: []const u8, concept: Concept) !void {
    var v: Janet = undefined;
    {
        const xs = try this.parse_statements(tt.allocator, input);
        defer xs.deinit();
        try tt.expectEqual(@as(usize, 1), xs.items.len);
        v = xs.items[0];
    }
    try tt.expect(concept.eql(try parse(v)));
}
