host: ?[]const u8 = null,
port: ?u16 = 5882,
unix_socket: ?[]const u8 = null,
statements_path: []const u8 = "statements",

pub const limits = .{
    .source_file_size = 1024 * 1024 * 16,
    .concept_member_count = 4,
};
