const std = @import("std");
const se = @import("se.zig");
const D = se.builtin_dictionary;
const ref = se.ref;
const imagine = se.imagine;

p_inotifywait: std.ChildProcess,

pub fn init(a: std.mem.Allocator, directory_name: []const u8) !@This() {
    var p = std.ChildProcess.init(&.{
        "inotifywait",
        "--monitor",
        "--quiet",
        "--event",
        "close_write,move,create,delete",
        "-r",
        directory_name,
    }, a);
    p.stdin_behavior = .Close;
    p.stdout_behavior = .Pipe;
    p.stderr_behavior = .Inherit;
    try p.spawn();
    _ = p.stdout.?; // check stdout is there
    imagine()
        .c(ref(0), .{ D.self, D.act, D.watch_statements })
        .state();
    return .{
        .p_inotifywait = p,
    };
}

pub fn deinit(this: *@This()) void {
    _ = this.p_inotifywait.kill() catch |e| {
        std.log.warn("failed to kill inotifywait; e={}", .{e});
    };
}

pub fn need_reload(this: *@This()) bool {
    const stdout = this.p_inotifywait.stdout.?;
    var buf: [4096]u8 = undefined;
    var has_data = false;
    while (true) {
        const n_read = try stdout.read(&buf);
        if (n_read > 0) {
            // only ratchet forward on every line break
            if (std.mem.indexOfScalar(u8, buf[0..n_read], '\n')) |_| {
                has_data = true;
            }
        } else {
            break;
        }
    }
    if (has_data) {
        imagine()
            .c(ref(0), .{ D.self, D.act, D.reload_statements })
            .state();
    }
    return has_data;
}
